package com.example.android_development_nutriphi.Prevalent;

import com.example.android_development_nutriphi.Model.Users;

public class Prevalent
{
    public static Users currentOnlineUser;

    public static final String UserPhoneKey = "UserPhone";
    public static final String UserPasswordKey = "UserPassword";
}
