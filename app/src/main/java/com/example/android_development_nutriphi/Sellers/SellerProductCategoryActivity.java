package com.example.android_development_nutriphi.Sellers;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.android_development_nutriphi.R;

public class SellerProductCategoryActivity extends AppCompatActivity {
    private ImageView aquaFeeding, cattleFeeding, chickenFeeding, swineFeeding;
    private ImageView aquaMedicine, cattleMedicine, chickenMedicine, swineMedicine;
    private ImageView aquaVaccine, cattleVaccine, chickenVaccine, swineVaccine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_product_category);

        aquaFeeding = findViewById(R.id.aqua_feed);
        cattleFeeding = findViewById(R.id.cattle_feed);
        chickenFeeding = findViewById(R.id.chicken_feed);
        swineFeeding = findViewById(R.id.swine_feed);

        aquaMedicine = findViewById(R.id.aqua_medicine);
        cattleMedicine = findViewById(R.id.cattle_medicine);
        chickenMedicine = findViewById(R.id.chicken_medicine);
        swineMedicine = findViewById(R.id.swine_medicine);

        aquaVaccine = findViewById(R.id.aqua_vaccine);
        cattleVaccine = findViewById(R.id.cattle_vaccine);
        chickenVaccine =findViewById(R.id.chicken_vaccine);
        swineVaccine = findViewById(R.id.swine_vaccine);

        aquaFeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Aqua Feeding");
                startActivity(intent);
            }
        });

        cattleFeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Cattle Feeding");
                startActivity(intent);
            }
        });

        chickenFeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Chicken Feeding");
                startActivity(intent);
            }
        });

        swineFeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Swine Feeding");
                startActivity(intent);
            }
        });

        aquaMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Aqua Medicine");
                startActivity(intent);
            }
        });

        cattleMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Cattle Medicine");
                startActivity(intent);
            }
        });

        chickenMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Chicken Medicine");
                startActivity(intent);
            }
        });

        swineMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Swine Medicine");
                startActivity(intent);
            }
        });

        aquaVaccine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Aqua Vaccine");
                startActivity(intent);
            }
        });

        cattleVaccine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Cattle Vaccine");
                startActivity(intent);
            }
        });

        chickenVaccine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Chicken Vaccine");
                startActivity(intent);
            }
        });

        swineVaccine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SellerProductCategoryActivity.this, SellerAddNewProductActivity.class);
                intent.putExtra("category", "Swine Vaccine");
                startActivity(intent);
            }
        });
    }
}
