package com.example.android_development_nutriphi.StaticPages;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.example.android_development_nutriphi.Buyers.HomeActivity;
import com.example.android_development_nutriphi.R;
import com.example.android_development_nutriphi.Buyers.SettingsActivity;

public class FeaturesActivity extends AppCompatActivity {
    private CardView cardView_1;
    private CardView cardView_2;
    private CardView cardView_3;
    private CardView cardView_4;
    private CardView cardView_6;
    private CardView cardView_7;
    private CardView cardView_8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_features);

        Toolbar toolbar = (Toolbar) findViewById(R.id.features_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("NutiPhi");

        cardView_1 = findViewById(R.id.first_card);
        cardView_2 = findViewById(R.id.second_card);
        cardView_3 = findViewById(R.id.third_card);
        cardView_4 = findViewById(R.id.fourth_card);
        cardView_6 = findViewById(R.id.sixth_card);
        cardView_7 = findViewById(R.id.seventh_card);
        cardView_8 = findViewById(R.id.eighth_card);

        cardView_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_1 = new Intent(FeaturesActivity.this, ScannerActivity.class);
                startActivity(intent_1);
            }
        });

        cardView_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_2 = new Intent(FeaturesActivity.this, ServicesActivity.class);
                startActivity(intent_2);
            }
        });

        cardView_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_3 = new Intent(FeaturesActivity.this, SpeciesActivity.class);
                startActivity(intent_3);
            }
        });
        cardView_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_4 = new Intent(FeaturesActivity.this, HomeActivity.class);
                startActivity(intent_4);

            }
        });

        cardView_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_6 = new Intent(FeaturesActivity.this, FeedbackActivity.class);
                startActivity(intent_6);
            }
        });

        cardView_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_7 = new Intent(FeaturesActivity.this, ContactUsActivity.class);
                startActivity(intent_7);
            }
        });

        cardView_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_8 = new Intent(FeaturesActivity.this, AboutUsActivity.class);
                startActivity(intent_8);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.profile:
                Intent intent = new Intent(FeaturesActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.exit:
                AlertDialog.Builder builder = new AlertDialog.Builder(FeaturesActivity.this);
                builder.setMessage("Do You Want To Exit?");
                builder.setCancelable(true);

                builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                });

                builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                break;
            default:
                Toast.makeText(this, "Default", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        super.onBackPressed();
    }
}
