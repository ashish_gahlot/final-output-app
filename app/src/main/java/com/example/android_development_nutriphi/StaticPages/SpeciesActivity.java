package com.example.android_development_nutriphi.StaticPages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.android_development_nutriphi.R;
import com.example.android_development_nutriphi.StaticPages.SpeciesInfo.AquaInfoActivity;
import com.example.android_development_nutriphi.StaticPages.SpeciesInfo.Cattle.CattleMenuActivity;
import com.example.android_development_nutriphi.StaticPages.SpeciesInfo.PoultryInfoActivity;
import com.example.android_development_nutriphi.StaticPages.SpeciesInfo.SwineInfoActivity;

public class SpeciesActivity extends AppCompatActivity {
    CardView first_card;
    CardView second_card;
    CardView third_card;
    CardView forth_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_species);

        Toolbar toolbar = findViewById(R.id.species_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Species");

        first_card = findViewById(R.id.poultry);
        second_card = findViewById(R.id.Cattle);
        third_card = findViewById(R.id.swine);
        forth_card = findViewById(R.id.aqua);

        first_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpeciesActivity.this, PoultryInfoActivity.class);
                startActivity(intent);
            }
        });

        second_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpeciesActivity.this, CattleMenuActivity.class);
                startActivity(intent);
            }
        });

        third_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpeciesActivity.this, SwineInfoActivity.class);
                startActivity(intent);
            }
        });

        forth_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpeciesActivity.this, AquaInfoActivity.class);
                startActivity(intent);
            }
        });

    }
}
