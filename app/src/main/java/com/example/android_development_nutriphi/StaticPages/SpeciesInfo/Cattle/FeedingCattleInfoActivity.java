package com.example.android_development_nutriphi.StaticPages.SpeciesInfo.Cattle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.android_development_nutriphi.R;

public class FeedingCattleInfoActivity extends AppCompatActivity {
    CardView cardView_1, cardView_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeding_cattle_info);

        Toolbar toolbar = findViewById(R.id.feeding_cattle_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feeding Cattle");

        cardView_1 = findViewById(R.id.cow);
        cardView_2 = findViewById(R.id.buffalo);

        cardView_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FeedingCattleInfoActivity.this, CowFeedingActivity.class));
            }
        });

        cardView_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FeedingCattleInfoActivity.this, BuffaloFeedingActivity.class));
            }
        });
    }
}
