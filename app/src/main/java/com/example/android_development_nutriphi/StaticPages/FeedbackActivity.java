package com.example.android_development_nutriphi.StaticPages;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.android_development_nutriphi.Prevalent.Prevalent;
import com.example.android_development_nutriphi.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class FeedbackActivity extends AppCompatActivity {
    private EditText nameFeedbacker, feedbackDescription;
    private Button submitFeedback;
    private ProgressDialog progressDialog;
    private RadioButton comments_, bug_report, questions_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        comments_ = findViewById(R.id.comments);
        bug_report = findViewById(R.id.bug_report);
        questions_ = findViewById(R.id.questions);

        progressDialog = new ProgressDialog(this);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = findViewById(R.id.feedback_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback");

        nameFeedbacker = findViewById(R.id.editText3);
        feedbackDescription = findViewById(R.id.editText2);
        submitFeedback = findViewById(R.id.button2);

        submitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String feedback_type = null;
                if(comments_.isChecked())
                {
                    feedback_type = "Comment";
                }
                if(bug_report.isChecked())
                {
                    feedback_type = "Bug Report";
                }
                if(questions_.isChecked())
                {
                    feedback_type = "Question";
                }

    //            String feedback_type = (String) radioButton.getText();
                String name = nameFeedbacker.getText().toString();
                String feedback_description = feedbackDescription.getText().toString();

                if(!name.equals("") && !feedback_description.equals("") && !feedback_type.equals(""))
                {
                    progressDialog.setTitle("Feedback");
                    progressDialog.setMessage("Please wait, your feedback is getting submit.");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();

                    final DatabaseReference feedbackRef;
                    feedbackRef = FirebaseDatabase.getInstance().getReference();

//                    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//                    String fid = firebaseUser.getDisplayName();
//                    String fid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                    HashMap<String, Object> feedback = new HashMap<>();
           //         feedback.put("fid", fid);
           //         feedback.put("feedback type", feedback_type);
            //        feedback.put("name", name);
                    feedback.put("Type", feedback_type);
                    feedback.put("Feedback Description", feedback_description);

                    feedbackRef.child("Users").child(Prevalent.currentOnlineUser.getPhone()).child("Feedback").updateChildren(feedback).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) 
                        {
                            progressDialog.dismiss();
                            Toast.makeText(FeedbackActivity.this, "Thank You for your feedback.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else 
                {
                    Toast.makeText(FeedbackActivity.this, "Please complete the feedback form.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
