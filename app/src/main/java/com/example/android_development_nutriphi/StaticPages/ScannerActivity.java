package com.example.android_development_nutriphi.StaticPages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android_development_nutriphi.R;

public class ScannerActivity extends AppCompatActivity {
    public ImageView scanBtn;
    public static TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        scanBtn = findViewById(R.id.scan_btn);
        result = findViewById(R.id.scan_result);

        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScannerActivity.this, ScanCodeActivity.class));
            }
        });

        Toolbar toolbar = findViewById(R.id.scanner_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Scanner");
    }
}
