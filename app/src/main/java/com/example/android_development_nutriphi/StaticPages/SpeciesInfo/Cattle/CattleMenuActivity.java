package com.example.android_development_nutriphi.StaticPages.SpeciesInfo.Cattle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.android_development_nutriphi.R;

public class CattleMenuActivity extends AppCompatActivity {
    CardView cardView_1, cardView_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cattle_menu);

        Toolbar toolbar = findViewById(R.id.cattle_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cattle");

        cardView_1 = findViewById(R.id.cattle_info);
        cardView_2 = findViewById(R.id.feeding_cattle);

        cardView_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_1 = new Intent(CattleMenuActivity.this, CattleInfoActivity.class);
                startActivity(intent_1);
            }
        });

        cardView_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_2 = new Intent(CattleMenuActivity.this, FeedingCattleInfoActivity.class);
                startActivity(intent_2);
            }
        });
    }
}
