package com.example.android_development_nutriphi.StaticPages.SpeciesInfo.Cattle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android_development_nutriphi.R;

public class BuffaloFeedingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buffalo_feeding);

        Toolbar toolbar = findViewById(R.id.buffalo_feeding_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Milch Buffalo");

        final RadioGroup radioGroup;
        final TextView textView_1, textView_2, textView_3;
        final RadioButton btn_1, btn_2, btn_3;
        Spinner milk_yield;
        milk_yield = findViewById(R.id.spinner);
        btn_1 = findViewById(R.id.high);
        btn_2 = findViewById(R.id.moderate);
        btn_3 = findViewById(R.id.low);

        textView_1 = findViewById(R.id.concentrate);
        textView_2 = findViewById(R.id.green_fodder);
        textView_3 = findViewById(R.id.dry_roughage);

        radioGroup = findViewById(R.id.radio_group);

        milk_yield.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getSelectedItem().toString().trim();
                switch (selected)
                {
                    case "3":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("1.6 Kg");
                                    textView_2.setText("30 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("2.5 Kg");
                                    textView_2.setText("15 Kg");
                                    textView_3.setText("6 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("3.2 Kg");
                                    textView_2.setText("5 Kg");
                                    textView_3.setText("7 Kg");
                                }
                            }
                        });
                        break;
                    case "6":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("3.7 Kg");
                                    textView_2.setText("30 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("4.5 Kg");
                                    textView_2.setText("15 Kg");
                                    textView_3.setText("6 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("5.25 Kg");
                                    textView_2.setText("5 Kg");
                                    textView_3.setText("7 Kg");
                                }
                            }
                        });
                        break;
                    case "9":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("5.6 Kg");
                                    textView_2.setText("30 Kg");
                                    textView_3.setText("3.5 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("6.5 Kg");
                                    textView_2.setText("15 Kg");
                                    textView_3.setText("6.5 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("7.4 Kg");
                                    textView_2.setText("5 Kg");
                                    textView_3.setText("7 Kg");
                                }
                            }
                        });
                        break;
                    case "12":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("7.6 Kg");
                                    textView_2.setText("30 Kg");
                                    textView_3.setText("4 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("8.5 Kg");
                                    textView_2.setText("15 Kg");
                                    textView_3.setText("7 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("9.4 Kg");
                                    textView_2.setText("5 Kg");
                                    textView_3.setText("7 Kg");
                                }
                            }
                        });
                        break;
                    default:
                        Toast.makeText(BuffaloFeedingActivity.this, "Some Error Occured", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
